import { Component, OnInit, AfterViewInit } from '@angular/core';
import * as esri from 'esri-leaflet';
import * as L from 'leaflet';
import { ApiService } from './api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {

  constructor(private apiService: ApiService) {}
  title = 'Test Maps';
  private map;

  private polygons = [];

  ngAfterViewInit(): void {
    this.initMap();
  }

  private initMap(): void {
    this.map = L.map('map').setView([37.667747 , -121.198148], 13);

    const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 19, 
    attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'});
    tiles.addTo(this.map);
    this.apiService.getAllPolygons().subscribe(data => {
      this.polygons = data;

      this.polygons.forEach( (data)=> {
        console.log('In the forEach', data.geo_json);
        L.geoJSON(data.geo_json).addTo(this.map);
      });
    });
  }
}
