import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { environment } from './../environments/environment';
import { Observable, throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': 'my-auth-token'
    })
  };

  private url = environment.polygonsUrl;

  constructor(private httpClient: HttpClient) { }

  createPolygon(name: string, geojson: any) {
    var polygon = {
      "name": name,
      "geojson": geojson
    };
    console.log(polygon);
    return this.httpClient.post<any>(this.url, JSON.stringify(polygon), this.httpOptions);
  }

  getAllPolygons(): Observable<any> {
    return this.httpClient.get(this.url);
  }
}
